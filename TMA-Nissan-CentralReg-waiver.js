var data, data1, data2, date;
var rowCar1, rowCar2,rowCar3, rowCar4;
var optIn = $("#optIn");
var dealerContact = $("#DealerContact");

function pageLoad() {
	$mz.onload();
	MZvalidStyle();
	checkBoxLimit(document.mozeus.model);
}

function pageLoad2(){
	$mz.onload();
	MZvalidStyle();
	checkFields();
}

$(function(){

    $('button').bind('click', function(e){

        var page = $(this).attr('data-page');

        $(this).addClass('push');
        $(this).one('webkitAnimationEnd animationend', function(){
            $(this).removeClass('push');

            if(page === 'start') {
                $('html').addClass('rev');
                $('.sidebar').removeClass('startWidth');
                $('.progress').removeClass('startProgress');
                $('.logo').removeClass('startLogo');
                $('h2').html('<span class="space">STEP 1.</span> <span class="split">FILL OUT YOUR INFORMATION</span>');

                $('.current').removeClass('current').next('span').addClass('current');
 
                $('#start').hide();
                $('#data').show();

                header();

                clearInterval(fanPulse);

            }else if(page === 'data'){

                ReportResult();

            }else if(page === 'waiverCopy'){

            	data += "signatureDate=" + date + ";";

            	SubmitResultString(data);

            }else if(page === 'waiverData'){

            	ReportResult2();

            }else if(page === 'minorData'){

            	ReportResult3();
            }else if(page === 'decline'){

            	SubmitResultString("AcceptTerms=Yes;");

            }else if(page === 'declineMinor'){

            	SubmitResultString("AcceptTerms1=Yes;");

            }else if(page === 'photoNo'){
            	if(soloPhoto){
            		SubmitResultString('Photo=No;')
            	}else{
            		data1 += 'Photo=No;';
            		SubmitResultString(data1);
            	}
            }else if(page === 'photoYes'){
            	if(soloPhoto){
             		SubmitResultString('Photo=Yes;')           		
            	}else{
            		data1 += 'Photo=Yes;';
            		SubmitResultString(data1);
            	}
            }
        });
    });//advance start

    header();

});//document.ready

function header(){
    $("h2 .split").each(function(index){
        var $div = $(this);
        var divWords = $div.text().split(/\s+/);
        $div.empty();
        $.each(divWords, function(i,w){
            $('<span class="animated pulse delay'+i+'" />').text(w + ' ').appendTo($div);
            i++;
        });

    });
}

var fanPulse = setInterval(function(){

    $(".fan").removeClass("pulse").delay(1000).queue(function(){ $(this).addClass("pulse").dequeue(); });

}, 8000);
		
// advance to next screen
function advance(id1,id2) {
$('#' + id1).addClass("fadeOut");
$('#' + id1).one('webkitAnimationEnd animationend', function(){
	$('#' + id1).removeClass("fadeOut");
	$('#' + id1).hide(); 
	// window.scrollTo(0,0);
	$('#' + id2).addClass('fadeIn');
	$('#' + id2).show();
});
$('#' + id2).one('webkitAnimationEnd animationend', function(){
	$('#' + id2).removeClass("fadeIn"); 
});

//lastPage = id1;
}

function displayContact(){
	$('#dealerContactWrapper').toggle();
}

function ReportResult() {
	if (validateFormOnSubmit()) {
		data = BuildResult(this.document, "mozeus");
		console.log(data)

	    var checkboxes = document.getElementsByName('model');
	    var checkCount = 1;
	    
	    for (var i=0; i<checkboxes.length; i++){
	        if(checkboxes[i].type == 'checkbox' && checkboxes[i].checked == true){
	            var checkID = (checkboxes[i].id).replace('mz_','');
	            data += "Model" + checkCount + "=" + checkID + ";";
	            console.log(data)

	            checkCount ++;
	        }
	    }

	    if(document.getElementById('DealerContact').checked){
	    	var interestVal = "interest=" + $('#mz_interest').val() + ';';
	    	var purchaseHorizonVal = "purchaseHorizon=" + $('#mz_purchaseHorizon').val() + ';';

	    	data += interestVal + purchaseHorizonVal;
	    }
	    
	    $('h2').html('<span class="space">STEP 2.</span> <span class="split">SIGN WAIVER</span>');
        $('.current').removeClass('current').next('span').addClass('current');
        getDate();
	    advance('data', 'waiverCopy');

	}else{
		console.log('not valid!')
		$("#data").animate({ scrollTop: 0 }, "slow");
	}
}

function validateFormOnSubmit(){
	var email = false;
	var mobile = false;
	var interest = true;
	var purchaseHorizon = true;
	var voi = true;
	var fname = $mz.validate.element($mz.find("FName"));
    var lname = $mz.validate.element($mz.find("LName"));
    var address  = $mz.validate.element($mz.find("Address"));
    var zip = $mz.validate.element($mz.find("Zip")) && checkZipCode(document.getElementById('Zip'));

    if($('#Email').val() === "" && $('#Mobile').val()  === ""){
    	if(document.getElementById('DealerContact').checked){
	    	console.log('contact dealer mobile')
	    	var mobile = $mz.validate.element($mz.find("Mobile"));
	    }else{
	    	var mobile = $mz.validate.element($mz.find("Mobile"));
        	var email = $mz.validate.element($mz.find("Email"));
	    }
    }

    if($('#Mobile').val() !== ""){
        var mobile = $mz.validate.element($mz.find("Mobile"));
        $('#Email').css('border', '.075rem solid #d0d0d0');
        email = true;
        if($('#Email').val() !== ""){
        	email = $mz.validate.element($mz.find("Email"));
        }
    }

    if($('#Email').val() !== ""){
    	if(document.getElementById('DealerContact').checked){
	    	console.log('contact dealer mobile')
	    	var mobile = $mz.validate.element($mz.find("Mobile"));
	    }else{
	    	var email = $mz.validate.element($mz.find("Email"));
        	$('#Mobile').css('border', '.075rem solid #d0d0d0');
        	mobile = true;
        	if($('#Mobile').val() !== ""){
	        	mobile = $mz.validate.element($mz.find("Mobile"));
	        }
	    }  
    }

    if(document.getElementById('DealerContact').checked){
    	var interest = $mz.validate.element($mz.find("mz_interest"));
    	var purchaseHorizon = $mz.validate.element($mz.find("mz_purchaseHorizon"));
    	if(checkChecks() >= 1){
    		voi = true;
    		console.log('voi checked!')
    	}else{
    		voi = false;
    		$('#voiLabel').css('color', 'red');
    		console.log('voi NOT checked!')
    	}
    }

    return fname && lname && zip && email && address && mobile && interest && purchaseHorizon && voi;
}

function standardZip(which) {

    if (which.value.length < 5) {
        console.log('zip length < 5');
        return false;
    } else {
        var cz = checkZipCode(which);
        console.log('checkZipCode='+cz);

        if (cz === false) {
            return false;
        }else {
            $('#City, #State').css('color', '#000');
            console.log('test');
            return true;
        }
    }
}//standardZip

function checkZipCode(which) {

    var zip = which.value;
    var validateZip = LoadAddress(zip);

    console.log('validateZip=' + validateZip);
    if (validateZip === null) {
        //use invalid class with !important to override passing MZ validation
        $(which).addClass('invalid');
        return false;
    } else {
        CheckZip();
        $(which).removeClass('invalid');
        return true;
    }

}//checkZipCode

function checkBoxLimit(checkBoxGroup) {
    var checkBoxGroup = checkBoxGroup;
    var limit = 2;
    for (var i = 0; i < checkBoxGroup.length; i++) {
        checkBoxGroup[i].onclick = function() {
            var checkedcount = 0;
            for (var i = 0; i < checkBoxGroup.length; i++) {
                checkedcount += (checkBoxGroup[i].checked) ? 1 : 0;
            }
            if (checkedcount > limit) {
                this.checked = false;
            }
        }
    }
}

function checkChecks(){
	var checks = $('.subModelInput');
	var checkCount = 0;

	for(var i = 0; i < checks.length; i++){
		if(checks[i].checked){
			checkCount++;
			console.log(checkCount);
		}
	}

	return checkCount;
}

function toggleUncheck(subMenuID, rowNum, voiID, carClass){

	if(rowNum === 1){
		rowCar1 = "rogueSelect";
		rowCar2 = "muranoSelect";
		rowCar3 = "altimaSelect";
		rowCar4 = "maximaSelect";
	}else if(rowNum === 2){
		rowCar1 = "jukeSelect";
		rowCar2 = "pathfinderSelect";
		rowCar3 = "xterraSelect";
		rowCar4 = "armadaSelect";
	}else if(rowNum === 3){
		rowCar1 = "sentraSelect";
		rowCar2 = "threeZSelect";
		rowCar3 = "versaSelect";
		rowCar4 = "gtrSelect";
	}else if(rowNum === 4){
		rowCar1 = "titanSelect";
		rowCar2 = "leafSelect";
		rowCar3 = "questSelect";
		rowCar4 = "NVSelect";
	}

	var $voiID = $('#' + voiID);
	var checkboxes = document.getElementsByClassName(carClass);

	if($voiID.prop('checked')){

		$('#voiMenu' + rowNum).show();
		$('#' + subMenuID).css('visibility', 'visible');

		if(checkboxes.length <= 1){

			if(checkChecks() < 2){
				for (var i = 0; i < checkboxes.length; i++){
		        	checkboxes[i].checked = true;
		    	}
			}
		}
		console.log('show!')
	}else{
		$('#' + subMenuID).css('visibility', 'hidden');

		model1 = $('#' + rowCar1).css('visibility') === 'hidden';
		model2 = $('#' + rowCar2).css('visibility') === 'hidden';
		model3 = $('#' + rowCar3).css('visibility') === 'hidden';
		model4 = $('#' + rowCar4).css('visibility') === 'hidden';

		if(voiID === "mz_Frontier"){
			$('#voiMenu' + rowNum).hide();
		}else if(model1 && model2 && model3 && model4){
			$('#voiMenu' + rowNum).hide();
		}

		

		console.log('not checked!');
    
	    for (var i = 0; i < checkboxes.length; i++){
	        if(checkboxes[i].checked){
	        	checkboxes[i].checked = false;
	        }
	    }
	}
}

$('#DealerContact').on('click', function(e){
	displayContact();
});


$('.models').on('click', function(e){
	
	var model = $(this).attr('id');
	switch (model) {
	    case 'mz_Rogue':
	        toggleUncheck('rogueSelect', 1, 'mz_Rogue', 'rogue');
	        break;
	    case 'mz_Murano':
	    	toggleUncheck('muranoSelect', 1, 'mz_Murano', 'murano');
	        break;
	    case 'mz_Altima':
	        toggleUncheck('altimaSelect', 1, 'mz_Altima', 'altima');
	        break;
	    case 'mz_Maxima':
	        toggleUncheck('maximaSelect', 1, 'mz_Maxima', 'maxima');
	        break;
	    case 'mz_Juke':
	        toggleUncheck('jukeSelect', 2, 'mz_Juke', 'juke');
	        break;
	    case 'mz_Pathfinder':
	        toggleUncheck('pathfinderSelect', 2, 'mz_Pathfinder', 'pathfinder');
	        break;
	    case  'mz_Xterra':
	        toggleUncheck('xterraSelect', 2, 'mz_Xterra', 'xterra');
	        break;
	    case  'mz_Armada':
	        toggleUncheck('armadaSelect', 2, 'mz_Armada', 'armada');
	        break;
	    case  'mz_Sentra':
	        toggleUncheck('sentraSelect', 3, 'mz_Sentra', 'sentra');
	        break;
	    case  'mz_370Z':
	        toggleUncheck('threeZSelect', 3, 'mz_370Z', 'threeZ');
	        break;
	    case  'mz_Versa':
	        toggleUncheck('versaSelect', 3, 'mz_Versa', 'versa');
	        break;
	    case  'mz_GTR':
	        toggleUncheck('gtrSelect', 3, 'mz_GTR', 'gtr');
	        break;
	    case  'mz_Titan':
	        toggleUncheck('titanSelect', 4, 'mz_Titan', 'titan');
	        break;
	    case  'mz_Leaf':
	        toggleUncheck('leafSelect', 4, 'mz_Leaf', 'leaf');
	        break;
	    case  'mz_NV':
	        toggleUncheck('NVSelect', 4, 'mz_NV', 'nvCar');
	        break;
	    case  'mz_Quest':
	        toggleUncheck('questSelect', 4,  'mz_Quest', 'quest');
	        break;
	    case  'mz_Frontier':
	        toggleUncheck('frontierSelect', 5, 'mz_Frontier', 'frontier')
	        break;
	}
});

function MZvalidStyle(){
    $("input, select").each(function( index ) {
        var id = $(this).attr('id');
        $(this).attr("data-json-override-invalid", "{'elemId':'" + id + "','invalidcss':'.075rem solid red','validcss':'.075rem solid #d0d0d0','property':'border'}")
    });
}//MZvalidStyle

$('.waiverWrap').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        $('#waiverCopySubmit').fadeIn();
    }
})

function getDate(){
	var $dateReplace = $('#dateReplace');

	var d = new Date();

	var month = d.getMonth() + 1;
	var day = d.getDate();
	var year = d.getFullYear();

	date = month + '/' + day + '/' + year;

	$dateReplace.html(date);
}

function checkFields(){
	if(fname.charAt(0) != "{"){
		$('#FName').val(fname).prop('disabled', true).addClass('grey').attr('id', 'mz_FName');
		$('#FNameMinor').val(fname).prop('disabled', true).addClass('grey').attr('id', 'mz_FNameMinor');
	}
	if(lname.charAt(0) != "{"){
		$('#LName').val(lname).prop('disabled', true).addClass('grey').attr('id', 'mz_LName');
		$('#LNameMinor').val(lname).prop('disabled', true).addClass('grey').attr('id', 'mz_LNameMinor');
	}
	if(email.charAt(0) != "{"){
		$('#Email').val(email).prop('disabled', true).addClass('grey').attr('id', 'mz_Email');
		$('#EmailDomain').prop('disabled', true);
	}
	if(mobile.charAt(0) != "{"){
		$('#Mobile').val(mobile).prop('disabled', true).addClass('grey').attr('id', 'mz_Mobile');
	}
	if(address.charAt(0) != "{"){
		$('#Address').val(address).prop('disabled', true).addClass('grey').attr('id', 'mz_Address');
	}
	if(zip.charAt(0) != "{"){
		$('#Zip').val(zip).prop('disabled', true).addClass('grey');
		standardZip(document.getElementById('Zip'));
	}
	if(city.charAt(0) != "{"){
		$('#City').prop('disabled', true).addClass('grey').attr('id', 'mz_City');
	}
	if(state.charAt(0) != "{"){
		$('#State').prop('disabled', true).addClass('grey').attr('id', 'mz_State');
	}
}

function ReportResult2() {
	if ($mz.validate.onsubmit("mozeus1")) {
		data1 = BuildResult(this.document, "mozeus1");
		console.log(data1)
       	
       	if($('#Minors').is(':checked')){
       		console.log('checked');
       		advance('waiverData', 'minorData');
       	}else{
       		$('h2').html('<span class="space">STEP 3.</span> <span class="split">TAKE YOUR PHOTO</span>');
        	$('.current').removeClass('current').next('span').addClass('current');

        	data1 += 'Minor=No;';

       		console.log('not checked');
       		advance('waiverData', 'photo');
       	}
	    
	}
}

function ReportResult3() {
	if (validateFormOnSubmit3()) {
		data2 = BuildResult(this.document, "mozeus2");

		data2 += 'Minor=Yes;';

		SubmitResultString(data1 + data2);
	}
}

function validateFormOnSubmit3(){
	// var fnameMinor = $mz.validate.element($mz.find("FNameMinor"));
	// console.log('fnameminor=' + fnameMinor)
	// var lnameMinor = $mz.validate.element($mz.find("LNameMinor"));
	// console.log('lnameMinor=' + lnameMinor)
	var fname1 = $mz.validate.element($mz.find("FName1"));
	var lname1 = $mz.validate.element($mz.find("LName1"));

	var fname2Val = $('#FName2').val();
	var fname3Val = $('#FName3').val();
	var lname2Val = $('#LName2').val();
	var lname3Val = $('#LName3').val();

	if(fname2Val != ""){
		$('#LName2').attr('data-optional' , 'false');
		console.log('last name 2 mandatory')
	}else{
		$('#LName2').attr('data-optional' , 'true');
	}

	if(lname2Val != ""){
		$('#FName2').attr('data-optional' , 'false');
		console.log('first name 2 mandatory')
	}else{
		$('#FName2').attr('data-optional' , 'true');
	}

	if(fname3Val != ""){
		$('#LName3').attr('data-optional' , 'false');
		console.log('last name 3 mandatory')
	}else{
		$('#LName3').attr('data-optional' , 'true');
	}

	if(lname3Val != ""){
		$('#FName3').attr('data-optional' , 'false');
		console.log('first name 3 mandatory')
	}else{
		$('#FName3').attr('data-optional' , 'true');
	}

	var fname2 = $mz.validate.element($mz.find("FName2"));
	var lname2 = $mz.validate.element($mz.find("LName2"));
	var fname3 = $mz.validate.element($mz.find("FName3"));
	var lname3 = $mz.validate.element($mz.find("LName3"));

	return fname1 && lname1 && fname2 && lname2 && fname3 && lname3;
}

// function SubmitResultString(result){console.log(result);}

