var $mz = $mz || {};
$mz = {
    _emailValidationTries: 0,
    _attemptedEmail: "",
    _absoluteMaxLength: 200,
    _emValidating: !1,
    deviceId: null,
    deviceLicenseToken: null,
    online: null,
    inputType: null,
    cfg: {
        frmElem: {
            data: {
                id: "mz_Data"
            },
            prefix: "mz_",
            fname: {
                id: "FName"
            },
            lname: {
                id: "LName"
            },
            dob: {
                month: "mz_BirthMonth",
                day: "mz_BirthDay",
                year: "mz_BirthYear"
            },
            address: {
                id: "Address"
            },
            city: {
                id: "City"
            },
            state: {
                id: "State"
            },
            zip: {
                id: "Zip"
            },
            country: {
                id: "Country"
            },
            gender: {
                id: "Gender"
            },
            county: {
                id: "County"
            },
            expiration: {
                month: "mz_ExpMonth",
                day: "mz_ExpDay",
                year: "mz_ExpYear"
            }
        },
        randomIDCharacters: "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789*$#@!-",
        settings: {
            emailValidationTimeout: 2e4,
            onload: {
                execute: [{
                    func: function() {
                        $mz.preventCopyPasteCut()
                    },
                    execute: !0
                }, {
                    func: function() {
                        $mz.resetDDLColorOnChange()
                    },
                    execute: !0
                }, {
                    func: function() {
                        $mz.addOnKeypUpValidation()
                    },
                    execute: !0
                }, {
                    func: function() {
                        $mz.convertEmailInputType()
                    },
                    execute: !0
                }]
            },
            maxAreaCode: 989,
            minAreaCode: 201
        },
        validation: {
            chars: {
                neverAllowed: [";", "=", "&"],
                zip: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                number: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                phone: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                name: ["-", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "'"],
                email: [".", "@", "_", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
                address: ["/", "#", ".", "-", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "'"],
                letternum: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
            },
            dataAttribute: {
                type: {
                    id: "data-type",
                    default: "Name"
                },
                allowExtendedValidation: {
                    id: "data-allow-extended",
                    default: {
                        name: !1,
                        email: !1,
                        address: !1,
                        zip: !1,
                        number: !1,
                        phone: !0,
                        freetext: !1,
                        exempt: !1,
                        letternum: !1
                    }
                },
                allowMask: {
                    id: "data-allow-mask",
                    default: {
                        name: !1,
                        email: !1,
                        address: !1,
                        zip: !1,
                        number: !1,
                        phone: !1,
                        freetext: !1,
                        exempt: !1,
                        letternum: !1
                    }
                },
                allowMidSpaces: {
                    id: "data-allow-midspaces",
                    default: {
                        name: !0,
                        email: !1,
                        address: !0,
                        zip: !1,
                        number: !1,
                        phone: !1,
                        freetext: !0,
                        exempt: !0,
                        letternum: !1
                    }
                },
                allowMultiple: {
                    id: "data-allow-multiple",
                    default: {
                        name: !1,
                        email: !1,
                        address: !1,
                        zip: !1,
                        number: !1,
                        phone: !1,
                        freetext: !0,
                        exempt: !0,
                        letternum: !1
                    }
                },
                capitalize: {
                    id: "data-capitalize",
                    default: {
                        name: "first",
                        email: "none",
                        address: "first",
                        zip: "none",
                        number: "none",
                        phone: "none",
                        freetext: "none",
                        exempt: "none",
                        letternum: "none"
                    }
                },
                format: {
                    submit: {
                        id: "data-format-submit",
                        default: {
                            phone: "##########",
                            zip: "#####"
                        }
                    },
                    display: {
                        id: "data-format-display",
                        default: {
                            phone: "##########",
                            zip: "#####"
                        }
                    }
                },
                invalidMessageElemId: {
                    id: "data-invalid-msgelementid",
                    default: {
                        email: null
                    }
                },
                jsonOverrideInvalid: {
                    id: "data-json-override-invalid",
                    default: null
                },
                jsonOverrideValue: {
                    id: "data-json-override-value",
                    default: null
                },
                maskCharacters: {
                    id: "data-mask-characters",
                    default: {
                        name: "",
                        email: "",
                        address: "",
                        zip: "",
                        phone: "(,),-,_, ",
                        freetext: "",
                        exempt: "",
                        letternum: "-,_"
                    }
                },
                maxLength: {
                    id: "data-max-length",
                    default: {
                        name: 200,
                        email: 200,
                        address: 200,
                        zip: 5,
                        number: 10,
                        phone: 10,
                        freetext: 200,
                        exempt: 200,
                        letternum: 6
                    }
                },
                minLength: {
                    id: "data-min-length",
                    default: {
                        name: 2,
                        email: 7,
                        address: 2,
                        zip: 5,
                        number: 10,
                        phone: 10,
                        freetext: 0,
                        exempt: 0,
                        letternum: 6
                    }
                },
                optional: {
                    id: "data-optional",
                    default: !1
                },
                removeMaskOnSubmit: {
                    id: "data-onsubmit-remove-mask",
                    default: !0
                },
                validateOnKeyUp: {
                    id: "data-validate-onkeyup",
                    default: {
                        name: !0,
                        email: !0,
                        address: !0,
                        zip: !0,
                        number: !0,
                        phone: !0,
                        freetext: !0,
                        exempt: !1,
                        letternum: !0
                    }
                },
                validateOnline: {
                    id: "data-validate-online",
                    default: !1
                },
                validateOnlineMaxTries: {
                    id: "data-validate-online-maxtries",
                    default: {
                        email: 1
                    }
                }
            },
            color: {
                valid: {
                    background: {
                        hex: "#ffffff",
                        rgb: "rgb(255,255,255)"
                    },
                    border: {
                        hex: "#ffffff",
                        rgb: "rgb(255,255,255)"
                    }
                },
                invalid: {
                    background: {
                        hex: "#FFFF00",
                        rgb: "rgb(255,255,0)"
                    },
                    border: {
                        hex: "#FF0000",
                        rgb: "rgb(255,0,0)"
                    }
                }
            }
        }
    },
    constants: {
        JSON_BASE_URL: "https://api.smartactivator.com/",
        ios: "iOS",
        android: "Android",
        windows: "Windows",
        mz_ignore_value: "MZ-IGNORE-RESULT-MZ"
    },
    addOnKeypUpValidation: function() {
        for (var e = document.getElementsByTagName("input"), t = function(e) {
                $mz.validate.onkeyup(this)
            }, a = function() {
                $mz.validate.onkeyup(this)
            }, n = 0; n < e.length; n++) "exempt" === $mz.validate.getValidationType(e[n]).toLowerCase() || !$mz.validate.getValidateOnKeyUp(e[n]) || "text" !== e[n].type.toLowerCase() && "tel" !== e[n].type.toLowerCase() && "email" !== e[n].type.toLowerCase() && "number" !== e[n].type.toLowerCase() && "hidden" !== e[n].type.toLowerCase() || $mz.attachEvent(e[n], "keyup", t, a);
        for (var i = document.getElementsByTagName("textarea"), l = 0; l < i.length; l++) $mz.attachEvent(i[l], "keyup", t, a)
    },
    attachEvent: function(e, t, a, n) {
        e.addEventListener ? e.addEventListener(t, a, !1) : e.attachEvent && e.attachEvent("on" + t, n)
    },
    capitalize: {
        all: function(e) {
            return e = e.toUpperCase()
        },
        first: function(e) {
            for (var t = e.split(" "), a = "", n = 0; n < t.length; n++) a += t[n].substring(0, 1).toUpperCase(), t[n].length > 1 && (a += t[n].substring(1)), t.length > 1 && n < t.length - 1 && (a += " ");
            e = a, t = e.split("-"), a = "";
            for (var n = 0; n < t.length; n++) a += t[n].substring(0, 1).toUpperCase(), t[n].length > 1 && (a += t[n].substring(1)), t.length > 1 && n < t.length - 1 && (a += "-");
            return a
        },
        firstword: function(e) {
            var t = e;
            if ("" !== e.trim())
                for (var a = "", n = 0; n < e.length; n++) {
                    if ($mz.isLetter(e[n], 1)) {
                        var i = e.substring(n, n + 1).toUpperCase();
                        t = a + i + e.substring(n + 1, e.length).toLowerCase();
                        break
                    }
                    a += e[n]
                }
            return t
        },
        freetext: function(e) {
            return e
        },
        none: function(e) {
            return e = e.toLowerCase()
        }
    },
    convertEmailInputType: function() {
        if ("android" === $mz.util.os().toLowerCase())
            for (var e = document.getElementsByTagName("input"), t = 0; t < e.length; t++) "email" === e[t].type.toLowerCase() && (e[t].type = "text")
    },
    escapeCodes: [{
        code: "&#13;",
        value: "\r"
    }, {
        code: "&#10;",
        value: "\n"
    }, {
        code: "&amp;",
        value: "&"
    }, {
        code: "&gt;",
        value: ">"
    }, {
        code: "&lt;",
        value: "<"
    }, {
        code: "&apos;",
        value: "'"
    }, {
        code: "&#39;",
        value: "'"
    }, {
        code: "&#38;",
        value: "&"
    }, {
        code: "&#62;",
        value: ">"
    }, {
        code: "&#60;",
        value: "<"
    }],
    find: function(e) {
        return document.getElementById(e)
    },
    getCursorPosition: function(e) {
        var t = 0;
        if (document.selection) {
            e.focus();
            var a = document.selection.createRange();
            a.moveStart("character", -e.value.length), t = a.text.length
        } else(e.selectionStart || "0 " === e.selectionStart) && (t = e.selectionStart);
        return t
    },
    getRandomString: function(e) {
        void 0 !== e && null !== e || (e = 20);
        for (var t = "", a = $mz.cfg.randomIDCharacters, n = 0; n < e; n++) t += a.charAt(Math.floor(Math.random() * a.length));
        return t
    },
    getUniqueGUID: function() {
        return function() {
            for (var e = {}, t = [], a = 0; a < 256; a++) t[a] = (a < 16 ? "0" : "") + a.toString(16);
            return e.generate = function() {
                var e = 4294967295 * Math.random() | 0,
                    a = 4294967295 * Math.random() | 0,
                    n = 4294967295 * Math.random() | 0,
                    i = 4294967295 * Math.random() | 0;
                return t[255 & e] + t[e >> 8 & 255] + t[e >> 16 & 255] + t[e >> 24 & 255] + "-" + t[255 & a] + t[a >> 8 & 255] + "-" + t[a >> 16 & 15 | 64] + t[a >> 24 & 255] + "-" + t[63 & n | 128] + t[n >> 8 & 255] + "-" + t[n >> 16 & 255] + t[n >> 24 & 255] + t[255 & i] + t[i >> 8 & 255] + t[i >> 16 & 255] + t[i >> 24 & 255]
            }, e
        }().generate()
    },
    hexToRGB: function(e) {
        var t = parseInt(e, 16);
        return (t >> 16 & 255) + ", " + (t >> 8 & 255) + ", " + (255 & t)
    },
    isAllTheSame: function(e) {
        e = e.toString();
        for (var t = 1; t < e.length; t++)
            if (e[t] !== e[t - 1]) return !1;
        return !0
    },
    isLetter: function(e) {
        return void 0 !== e && null !== e && /^[a-zA-Z]*$/.test(e)
    },
    isNumeric: function(e, t) {
        return !(void 0 === e || null === e || e.replace(" ", "").length !== t || isNaN(e) || !isFinite(e))
    },
    onload: function() {
        for (var e = $mz.cfg.settings.onload.execute, t = 0; t < e.length; t++) e[t].execute === !0 && e[t].func()
    },
    padLeft: function(e, t, a) {
        var n = "";
        if (void 0 !== e && null !== e)
            for (n = e.toString(), t = t || "0"; n.length < a;) n = t + n;
        return n
    },
    preventCopyPasteCut: function() {
        for (var e = document.getElementsByTagName("*"), t = function(e) {
                e.preventDefault()
            }, a = function() {
                return !1
            }, n = 0; n < e.length; n++) "exempt" !== $mz.validate.getValidationType(e[n]).toLowerCase() && ($mz.attachEvent(e[n], "copy", t, a), $mz.attachEvent(e[n], "paste", t, a), $mz.attachEvent(e[n], "cut", t, a))
    },
    resetDDLColorOnChange: function() {
        for (var e = document.getElementsByTagName("select"), t = function(e) {
                this.style.backgroundColor = $mz.cfg.validation.color.valid.background.hex
            }, a = function() {
                this.style.backgroundColor = $mz.cfg.validation.color.valid.background.hex
            }, n = 0; n < e.length; n++) "select-one" === e[n].type && e[n].id.toLowerCase().indexOf("emaildomain") == -1 && $mz.attachEvent(e[n], "change", t, a)
    },
    setCursorPosition: function(e, t) {
        try {
            if (e.setSelectionRange) e.focus(), e.setSelectionRange(t, t);
            else if (e.createTextRange) {
                var a = e.createTextRange();
                a.collapse(!0), a.moveEnd("character", t), a.moveStart("character", t), a.Select()
            }
        } catch (e) {}
    },
    setElement: function(e, t) {
        var a = $mz.find(e);
        if (void 0 !== a && null !== a) {
            var n = $mz.validate.getValidationType(a).toLowerCase();
            "text" !== a.type.toLowerCase() && "textarea" !== a.type.toLowerCase() && "tel" !== a.type.toLowerCase() && "email" !== a.type.toLowerCase() && "number" !== a.type.toLowerCase() && "hidden" !== a.type.toLowerCase() || "name" !== n && "city" !== n && "address" !== n && "email" !== n && "phone" !== n && "number" !== n && "exempt" !== n && "freetext" !== n ? "zip" === n ? $mz.validate.setZipElem(a, t) : "select-one" === a.type && "state" === n ? $mz.validate.setStateElem(a, t) : "select-one" === a.type && "gender" === n ? $mz.validate.setGenderElem(a, t) : "select-one" !== a.type || "month" !== n && "day" !== n && "year" !== n ? "select-one" === a.type ? $mz.validate.setElemValue(a, t) : "checkbox" === a.type && $mz.validate.setCheckBox(a, t) : $mz.validate.setElemValue(a, t) : $mz.validate.setTextElem(a, t)
        }
    },
    unescape: function(e) {
        for (var t = $mz.escapeCodes, a = 0; a < t.length; a++) e = e.replace(t[a].code, t[a].value);
        return e
    },
    util: {
        online: function() {
            return void 0 !== $mz.online && null !== $mz.online && "online" === $mz.online.toLowerCase().trim()
        },
        deviceId: function() {
            return void 0 !== $mz.deviceId && null !== $mz.deviceId && $mz.deviceId.toLowerCase().indexOf("{") === -1 ? $mz.deviceId : null
        },
        deviceLicenseToken: function() {
            return void 0 !== $mz.deviceLicenseToken && null !== $mz.deviceLicenseToken && $mz.deviceLicenseToken.toLowerCase().indexOf("{") === -1 ? $mz.deviceLicenseToken : null
        },
        os: function() {
            return navigator.appVersion.toLowerCase().indexOf("android") > -1 || navigator.appVersion.toLowerCase().indexOf("linux") > -1 ? $mz.constants.android : navigator.appVersion.toLowerCase().indexOf("ipad") > -1 || navigator.appVersion.toLowerCase().indexOf("iphone") > -1 || navigator.appVersion.toLowerCase().indexOf("mac") > -1 ? $mz.constants.ios : navigator.appVersion.toLowerCase().indexOf("windows") > -1 ? $mz.constants.windows : null
        },
        callService: function(e) {
            try {
                $.ajax({
                    url: e.url,
                    type: e.type,
                    data: e.data,
                    dataType: e.dataType,
                    header: e.header,
                    beforeSend: e.beforeSendCallback,
                    error: function(t) {
                        "json" === e.dataType ? console.log("ERROR: " + JSON.stringify(t)) : console.log("ERROR: " + t), void 0 !== e.onError && e.onError(t)
                    },
                    success: function(t) {
                        void 0 !== e.onSuccess && e.onSuccess(t)
                    },
                    timeout: e.timeout
                })
            } catch (t) {
                console.log(t.message), e.callback({
                    result: !1,
                    data: null
                })
            }
        },
        distance: function(e, t, a, n, i) {
            var l = Math.PI * e / 180,
                r = Math.PI * a / 180,
                o = (Math.PI, Math.PI, t - n),
                d = Math.PI * o / 180,
                s = Math.sin(l) * Math.sin(r) + Math.cos(l) * Math.cos(r) * Math.cos(d);
            return s = Math.acos(s), s = 180 * s / Math.PI, s = 60 * s * 1.1515, "K" === i && (s *= 1.609344), "N" === i && (s *= .8684), s.toFixed(1)
        },
        getServiceError: function(e) {
            try {
                if (0 < e.responseText.length) {
                    return jQuery.parseJSON(e.responseText).status
                }
                return e.status
            } catch (e) {
                console.log(e.message)
            }
            return 0
        }
    },
    validate: {
        address: function(e) {
            num = /[0-9]/, letter = /[A-Za-z]/;
            var t = e.value.trim(),
                a = t.length,
                n = 0;
            for (i = 0; i < a; i++) " " === t[i] && n++;
            return !(n < 1 || a < 4 || !num.test(t) || !letter.test(t))
        },
        ageRange: function(e, t, a) {
            var n = new Date,
                i = new Date(e),
                l = n.getFullYear() - i.getFullYear(),
                r = n.getMonth() - i.getMonth();
            return (r < 0 || 0 === r && n.getDate() < i.getDate()) && l--, l > a ? "Over" : l >= t && l <= a ? "In" : "Under"
        },
        capitalize: function(e) {
            if ("" !== e.value.trim() && "exempt" !== $mz.validate.getValidationType(e).toLowerCase()) {
                var t = $mz.validate.getCapitalize(e).toLowerCase().trim();
                val = $mz.capitalize[t](e.value), $mz.validate.setElemValue(e, val)
            }
        },
        date: {
            dropdowns: function(e, t, a) {
                var n = $mz.validate.setColor;
                n(e, !0), n(t, !0), n(a, !0);
                var i = $mz.validate.element(e),
                    l = $mz.validate.element(t),
                    r = $mz.validate.element(a);
                if (i && l && r) {
                    var o = $mz.padLeft(e.options[e.selectedIndex].value, "0", 2) + "/" + $mz.padLeft(t.options[t.selectedIndex].value, "0", 2) + "/" + a.options[a.selectedIndex].value,
                        d = $mz.validate.date.string(o);
                    return d || n(t, !1), d
                }
                return !1
            },
            string: function(e) {
                try {
                    var t = e.split("/");
                    if (3 === t.length) {
                        var a = parseInt(t[0]),
                            n = parseInt(t[1]),
                            i = parseInt(t[2]),
                            l = i % 4 == 0;
                        return !(!l && n > 28 && 2 === a) && (!(l && n > 29 && 2 === a) && (!(n > 30) || 4 !== a && 6 !== a && 9 !== a && 11 !== a))
                    }
                    return !1
                } catch (e) {
                    return !1
                }
            },
            twoDates: function(e, t, a) {
                try {
                    var n = e.split("/"),
                        i = t.split("/"),
                        l = void 0 === a || null === a || a;
                    if (3 === n.length && 3 === i.length) {
                        var r = parseInt(n[0]),
                            o = parseInt(n[1]),
                            d = parseInt(n[2]),
                            s = parseInt(i[0]),
                            u = parseInt(i[1]),
                            m = parseInt(i[2]),
                            c = new Date(d, r - 1, o, 0, 0, 0, 0),
                            v = new Date(m, s - 1, u, 0, 0, 0, 0);
                        return l ? c <= v : c < v
                    }
                } catch (e) {
                    return !1
                }
            }
        },
        dropdownElement: function(e) {
            var t = !0;
            return $mz.validate.getOptional(e) || void 0 === e.selectedIndex || "" !== e.options[e.selectedIndex].value || (t = !1), t
        },
        element: function(e, t) {
            var a = !0;
            return t = null !== t && t, "text" === e.type.toLowerCase() || "textarea" === e.type.toLowerCase() || "tel" === e.type.toLowerCase() || "email" === e.type.toLowerCase() || "number" === e.type.toLowerCase() ? a = $mz.validate.textElement(e, t) : "select-one" === e.type.toLowerCase() && (a = $mz.validate.dropdownElement(e)), $mz.validate.setColor(e, a), a
        },
        email: function(e) {
            var t = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/;
            if ($mz.validate.getAllowMultiple(e)) {
                for (var a = e.value.split(","), n = 0; n < a.length; n++)
                    if (!t.test(a[n])) return !1
            } else if (!t.test(e.value)) return !1;
            return !0
        },
        emailByOnlineService: function(e, t) {
            function a(e) {
                e < s.length ? $mz.validate._emailOnline(s[e], function(t) {
                    t || (d = !1), e++, a(e)
                }) : (t(d), $mz._emValidating = !1)
            }
            if ($mz._emValidating === !1) {
                $mz._emValidating = !0;
                var n = $mz.util.online(),
                    i = $mz.util.os(),
                    l = $mz.validate.getOptional(e),
                    r = e.value.trim();
                if (l && "" === r || !n || null === i || i === $mz.constants.ios && null === $mz.util.deviceLicenseToken() || i === $mz.constants.android && null === $mz.util.deviceId() || i === $mz.constants.windows && null === $mz.util.deviceLicenseToken()) t(!0), $mz._emValidating = !1;
                else if (l || "" !== r) {
                    var o = !!n && $mz.validate.getValidateOnline(e);
                    if (o) {
                        var d = !0,
                            s = r.split(","),
                            u = $mz._emailValidationTries,
                            m = $mz.validate.getValidateOnlineMaxTries(e);
                        r !== $mz._attemptedEmail || u < m ? (r !== $mz._attemptedEmail && ($mz._emailValidationTries = 0), $mz._attemptedEmail = r, a(0)) : (t(!0), $mz._emValidating = !1)
                    } else t(!0), $mz._emValidating = !1
                } else t(!1), $mz._emValidating = !1
            }
        },
        _emailOnline: function(e, t) {
            function a(e) {
                t(!0)
            }

            function n(e) {
                if ("invalid" === e.validation.toLowerCase()) return $mz._emailValidationTries++, void t(!1);
                t(!0)
            }

            function i(e) {
                null === l.token || $mz.util.os() !== $mz.constants.ios && $mz.util.os() !== $mz.constants.windows || e.setRequestHeader("Authorization", "Bearer " + l.token)
            }
            if ("undefined" != typeof $ && null !== $) try {
                var l = {
                        token: $mz.util.deviceLicenseToken()
                    },
                    r = $mz.util.os() === $mz.constants.android ? {
                        emailAddress: e,
                        deviceId: $mz.util.deviceId()
                    } : {
                        emailAddress: e
                    },
                    o = {
                        url: $mz.constants.JSON_BASE_URL + "consumer/validateemail",
                        type: "POST",
                        data: r,
                        dataType: "json",
                        beforeSendCallback: i,
                        onSuccess: n,
                        onError: a,
                        timeout: $mz.cfg.settings.emailValidationTimeout
                    };
                $mz.util.callService(o)
            } catch (e) {
                console.log(e.message), t(!0)
            } else console.log("No $"), t(!0)
        },
        getAllowExtendedValidation: function(e) {
            return $mz.validate.getDataAttr(e, "allowExtendedValidation")
        },
        getAllowMask: function(e) {
            return $mz.validate.getDataAttr(e, "allowMask")
        },
        getAllowMidSpaces: function(e) {
            return $mz.validate.getDataAttr(e, "allowMidSpaces")
        },
        getAllowMultiple: function(e) {
            return $mz.validate.getDataAttr(e, "allowMultiple")
        },
        getCapitalize: function(e) {
            return $mz.validate.getDataAttr(e, "capitalize")
        },
        getDataAttr: function(e, t) {
            var a = $mz.cfg.validation.dataAttribute,
                n = e.getAttribute(a[t].id);
            if (null === n && void 0 !== a[t].default) {
                var i = $mz.validate.getValidationType(e);
                n = null !== a[t].default && void 0 !== a[t].default[i.toLowerCase()] ? a[t].default[i.toLowerCase()] : a[t].default
            }
            return null !== n && "string" == typeof n && (n = "true" === n.toLowerCase() || "false" !== n.toLowerCase() && n), n
        },
        getInvalidMsgElemId: function(e) {
            return $mz.validate.getDataAttr(e, "invalidMessageElemId")
        },
        getJSONOverrideInvalid: function(e) {
            return $mz.validate.getDataAttr(e, "jsonOverrideInvalid")
        },
        getJSONOverrideValue: function(e) {
            return $mz.validate.getDataAttr(e, "jsonOverrideValue")
        },
        getMaskCharacters: function(e) {
            return $mz.validate.getDataAttr(e, "maskCharacters")
        },
        getMaxLength: function(e) {
            return $mz.validate.getDataAttr(e, "maxLength")
        },
        getMinLength: function(e) {
            return $mz.validate.getDataAttr(e, "minLength")
        },
        getOptional: function(e) {
            return $mz.validate.getDataAttr(e, "optional")
        },
        getRemoveMask: function(e) {
            return $mz.validate.getDataAttr(e, "removeMaskOnSubmit")
        },
        getValidateOnKeyUp: function(e) {
            return $mz.validate.getDataAttr(e, "validateOnKeyUp")
        },
        getValidateOnline: function(e) {
            return $mz.validate.getDataAttr(e, "validateOnline")
        },
        getValidateOnlineMaxTries: function(e) {
            return $mz.validate.getDataAttr(e, "validateOnlineMaxTries")
        },
        getValidationType: function(e) {
            var t = $mz.cfg.validation.dataAttribute,
                a = e.getAttribute(t.type.id);
            return null === a && (a = t.type.default), a
        },
        getValidValue: function(e, t, a) {
            a = void 0 === a || null === a ? $mz.validate.getValidationType(e).toLowerCase() : a, void 0 !== t && null !== t || (t = e.value);
            var n = $mz.validate.getMaxLength(e);
            if (!$mz.validate.getAllowMultiple(e)) return t = $mz.validate._getValidValue(e, t, a);
            n = $mz._absoluteMaxLength;
            for (var i = t.split(","), l = [], r = 0; r < i.length; r++) l.push($mz.validate._getValidValue(e, i[r], a));
            t = "";
            for (var r = 0; r < l.length; r++) t += l[r], r < l.length - 1 && (t += ",");
            return t = t.substring(0, n)
        },
        _getValidValue: function(e, t, a) {
            try {
                a = void 0 === a || null === a ? $mz.validate.getValidationType(e).toLowerCase() : a;
                var n = $mz.validate.getMaxLength(e);
                if (void 0 !== t && null !== t || (t = e.value), "exempt" === a) return t = t.substring(0, n);
                if ("freetext" === a) return t = $mz.validate.removeNeverAllowed(t), t = t.substring(0, n);
                for (var i = $mz.cfg.validation.chars[a.toLowerCase()], l = new Array(i.length), r = 0; r < i.length; r++) l[r] = i[r];
                var o = $mz.validate.getAllowMidSpaces(e);
                if ($mz.validate.getAllowMask(e)) {
                    var d = $mz.validate.getMaskCharacters(e).split(",");
                    n += d.length;
                    for (var s = 0; s < d.length; s++) " " === d[s] && (o = !0), l.push(d[s])
                }
                for (; 0 === t.indexOf(" ");) t = t.substr(1);
                for (; t.indexOf("  ") > 0;) t = t.replace("  ", " ");
                for (var u = !1, m = "", c = t, v = c.length, s = 0; s < v; s++) {
                    u = !1, m = c.charAt(s), 0 === s && " " === m && (t = t.trim());
                    for (var f = 0; f < l.length; f++)
                        if (m === l[f]) {
                            u = !0;
                            break
                        }
                    if (!u && o && " " !== m)
                        for (; t.indexOf(m) > -1;) t = t.replace(m, "");
                    else if (!u && !o)
                        for (; t.indexOf(m) > -1;) t = t.replace(m, "")
                }
                if (!o || "email" === a)
                    for (; t.indexOf(" ") > 0;) t = t.replace(" ", "");
                "email" === a && (t = t.trim()), t = t.substring(0, n)
            } catch (e) {
                alert(e.message)
            }
            return t
        },
        onkeyup: function(e) {
            try {
                var t = $mz.validate.getValidationType(e).toLowerCase();
                if ("exempt" !== t) {
                    var a = "text";
                    "email" === e.type.toLowerCase() && (a = "email", e.type = "text");
                    var n = $mz.getCursorPosition(e),
                        i = $mz.validate.getValidValue(e);
                    if (i === e.value) {
                        if ($mz.validate.setColor(e, !0), "email" === t) {
                            var l = $mz.validate.getInvalidMsgElemId(e);
                            null !== l && ($mz.find(l).style.display = "none")
                        }
                    } else e.value = i, n--;
                    $mz.validate.capitalize(e), $mz.setCursorPosition(e, n), "email" === a && (e.type = "email")
                }
            } catch (e) {
                alert(e.message)
            }
        },
        onsubmit: function(e, t) {
            function a(e) {
                function d(t) {
                    return null !== u && ($mz.find(u).style.display = t ? "none" : "inline"), i = i ? t : i, $mz.validate.setColor(r[o], t), e++, a(e), i
                }
                if (e < r.length) {
                    var s = $mz.validate.getValidationType(r[e]).toLowerCase();
                    if ("exempt" === s) return e++, a(e), i;
                    if (l = n.element(r[e]), "email" !== s || !l || "" === r[e].value) return i = i ? l : i, e++, a(e), i;
                    o = e;
                    var u = n.getInvalidMsgElemId(r[o]);
                    u = void 0 !== u && null !== u && "" !== u.trim() ? u : null, null !== u && ($mz.find(u).style.display = "none"), n.emailByOnlineService(r[o], d)
                } else {
                    if (void 0 === t || null === t) return i;
                    t(i)
                }
            }
            var n = $mz.validate,
                i = !0,
                l = !0,
                r = $mz.find(e).elements,
                o = ($mz.cfg.validation.color, -1);
            return a(0)
        },
        phone: function(e) {
            for (var t = $mz.validate, a = t.removeMask(e), n = a.split(","), i = t.getMinLength(e), l = t.getMaxLength(e), r = 0; r < n.length; r++) {
                if (!$mz.isNumeric(n[r], i) || !$mz.isNumeric(n[r], l)) return !1;
                if (t.getAllowExtendedValidation(e) && !t.phoneNumber(n[r])) return !1
            }
            return !0
        },
        phoneNumber: function(e) {
            var t = parseInt(e.substring(0, 3));
            return t >= $mz.cfg.settings.minAreaCode & t <= $mz.cfg.settings.maxAreaCode & !$mz.isAllTheSame(e) & !$mz.isAllTheSame(e.substring(3))
        },
        radioButtonGroup: function(e) {
            for (var t = document.getElementsByName(e), a = 0; a < t.length; a++)
                if (t[a].checked) return !0;
            return !1
        },
        removeNeverAllowed: function(e) {
            var t = $mz.cfg.validation.chars.neverAllowed;
            if (void 0 !== t && null !== t)
                for (var a = 0; a < t.length; a++)
                    for (; e.indexOf(t[a]) > -1;) e = e.replace(t[a], "");
            return e
        },
        removeMask: function(e) {
            var t;
            try {
                var a = $(e).data("kendoMaskedTextBox");
                if (void 0 !== a && null !== a) {
                    return t = a.raw()
                }
            } catch (e) {}
            t = e.value;
            for (var n = $mz.validate.getMaskCharacters(e).split(","), i = 0; i < n.length; i++)
                for (; t.indexOf(n[i]) > -1;) t = t.replace(n[i], "");
            return t
        },
        removeDataTagAndNull: function(e) {
            for (var t = $mz.find(e).elements, a = "", n = 0; n < t.length; n++)
                if ("text" === (a = t[n].type.toLowerCase()) || "tel" === a || "hidden" === a || "textarea" === a || "number" === a || "email" === a || "hidden" === a) {
                    var i = t[n].value.trim();
                    (i.indexOf("{") > -1 && i.indexOf("}") > -1 || "ull" === i.toLowerCase() || "null" === i.toLowerCase() || "undefined" === i.toLowerCase()) && (t[n].value = "")
                }
        },
        setCheckBox: function(e, t) {
            "yes" === t.toString().toLowerCase() || "true" === t.toString().toLowerCase() ? e.checked = !0 : e.checked = !1
        },
        setColor: function(e, t) {
            var a = $mz.validate.getJSONOverrideInvalid(e);
            if (null === a) return void(e.style.backgroundColor = t ? $mz.cfg.validation.color.valid.background.hex : $mz.cfg.validation.color.invalid.background.hex);
            a = a.replace(/'/g, '"'), a = JSON.parse(a);
            var n = a.invalidcss,
                i = a.validcss,
                l = a.property;
            e.style[l] = t ? i : n
        },
        setCountryElem: function(e, t) {},
        setCountyElem: function(e, t) {},
        setDateElems: function(e, t) {
            if ($mz.isNumeric(t, 8)) {
                var a = $mz.find(e.month),
                    n = $mz.find(e.day),
                    i = $mz.find(e.year),
                    l = t.substring(0, 2),
                    r = t.substring(2, 4),
                    o = t.substring(4, 8);
                if ($mz.validate.setElemValue(a, l), $mz.validate.setElemValue(n, r), "2099" === o && i.id === $mz.cfg.frmElem.expiration.year) {
                    for (var d = !1, s = 0; s < i.options.length; s++) "2099" === i.options[s].value && (d = !0);
                    if (!d) {
                        var u = document.createElement("option");
                        u.text = "2099", u.value = "2099", i.insertBefore(u, ddl.childNodes[2]), i.selectedIndex = 0
                    }
                }
                $mz.validate.setElemValue(i, o)
            }
        },
        setElemValue: function(e, t) {
            try {
                void 0 !== e && null !== e && void 0 !== t && null !== t && (e.value = t)
            } catch (e) {}
        },
        setGenderElem: function(e, t) {
            try {
                t = t.toLowerCase();
                var a = 1,
                    n = 2,
                    i = $mz.validate.getJSONOverrideValue(e);
                void 0 !== i && null !== i && (i = i.replace(/'/g, '"'), i = JSON.parse(i), a = i.male, n = i.female), t = "m" === t || "male" === t || "1" === t ? a : "f" === t || "female" === t || "2" === t ? n : "", $mz.validate.setElemValue(e, t)
            } catch (e) {
                e.message
            }
        },
        setStateElem: function(e, t) {
            if (void 0 !== e && null !== e && void 0 !== t && null !== t) {
                t = t.toUpperCase();
                var a = [];
                a.AL = "1", a.AK = "2", a.AZ = "3", a.AR = "4", a.CA = "5", a.CO = "6", a.CT = "7", a.DE = "8", a.FL = "9", a.GA = "10", a.HI = "11", a.ID = "12", a.IL = "13", a.IN = "14", a.IA = "15", a.KS = "16", a.KY = "17", a.LA = "18", a.ME = "19", a.MD = "20", a.MA = "21", a.MI = "22", a.MN = "23", a.MS = "24", a.MO = "25", a.MT = "26", a.NE = "27", a.NV = "28", a.NH = "29", a.NJ = "30", a.NM = "31", a.NY = "32", a.NC = "33", a.ND = "34", a.OH = "35", a.OK = "36", a.OR = "37", a.PA = "38", a.RI = "39", a.SC = "40", a.SD = "41", a.TN = "42", a.TX = "43", a.UT = "44", a.VT = "45", a.VA = "46", a.WA = "47", a.WV = "48", a.WI = "49", a.WY = "50", a.DC = "51", a.AB = "52", a.BC = "53", a.MB = "54", a.NB = "55", a.NL = "56", a.NS = "57", a.NT = "58", a.NU = "59", a.ON = "60", a.PE = "61", a.QC = "62", a.SK = "63", a.YT = "64";
                try {
                    $mz.validate.setElemValue(e, a[t])
                } catch (e) {}
            }
        },
        setTextElem: function(e, t) {
            if (void 0 !== e && null !== e && "exempt" !== $mz.validate.getValidationType(e).toLowerCase()) {
                var a = $mz.validate.getMaxLength(e);
                t = t.substring(0, a);
                $mz.validate.getValidValue(e, t) === t && (e.value = t, $mz.validate.capitalize(e))
            } else e.value = t
        },
        setZipElem: function(e, t) {
            var a = $mz.validate.getMaxLength(e),
                t = t.substring(0, a);
            $mz.isNumeric(t, a) && $mz.validate.setTextElem(e, t)
        },
        textElement: function(e, t) {
            var a = !0;
            if ("text" === e.type.toLowerCase() || "textarea" === e.type.toLowerCase() || "tel" === e.type.toLowerCase() || "email" === e.type.toLowerCase() || "number" === e.type.toLowerCase() || "hidden" === e.type.toLowerCase()) {
                var n = $mz.validate,
                    i = n.getValidationType(e).toLowerCase();
                if ("exempt" !== i) {
                    var l = n.getOptional(e),
                        r = n.getAllowMask(e) & n.getRemoveMask(e),
                        o = r ? n.removeMask(e) : e.value;
                    "" === o.trim() || n.getAllowMultiple(e) ? l || "" !== o.trim() || (a = !1) : a = e.value === n.getValidValue(e) && o.length <= n.getMaxLength(e) && o.length >= n.getMinLength(e), a && "" !== o.trim() && ("email" === i ? a = n.email(e) : "address" === i ? a = n.address(e) : "phone" === i && (a = n.phone(e)))
                }
            }
            return a
        },
        zip: function(e) {
            var t = $mz.validate,
                a = e.value,
                n = $mz.isNumeric(a, t.getMinLength(e));
            return n = n ? $mz.isNumeric(a, t.getMaxLength(e)) : n
        }
    }
}, $mz.deviceLicenseToken = "{DEVICELICENSETOKEN}", $mz.deviceId = "{DEVICEID}", $mz.online = "{ONLINE}";