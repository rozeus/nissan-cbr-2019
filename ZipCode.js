var valColor = "";
var invalColor = "Yellow";
function LoadAddress(zip)
{
    try {
        var req = new XMLHttpRequest();
        var zipFile = '';
        var status = 0;
        if (typeof OS !=="undefined" && typeof OS ==="function" && (OS("Android") || OS("Mac"))) {
            zipFile = 'zip_' + zip.substring(0, 1) + '.csv';
        }
        else {
            zipFile = 'js/zipcodes/zip_' + zip.substring(0, 1) + '.csv';
            status = 200;
        }
        req.open('GET', zipFile, false);
        req.send(null);
        if(req.status === status)
        {
            var csv = req.responseText;
            //alert(csv);
            var rows = csv.split("\n");
            var table = new Array(rows.length);

            //alert("Rows: " + rows.length);
            for (var i = 0; i < rows.length; i++)
            {
                table[i] = rows[i].split(",");
            }

            for (var j = 0; j < table.length; j++)
            {
                if (table[j][0] == zip)
                {
                    var address=new Object();
                    var aliases = table[j][3];

                    if (aliases.trim() != '')
                    {
                        aliases = '|' + aliases;
                    }

                    address.city=table[j][1] + aliases;
                    address.city = address.city.split("|");
                    address.state = table[j][2];

                    return address;
                }
            }

            return null;
        }
    }
    catch (e) {
        console.log(e.message);
    }
}

function GetStateID(abb)
{
    console.log("GetStateID");
    if (abb == 'AL')
        return 1;
    if (abb == "AK")
        return 2;
    if (abb == "AZ")
        return 3;
    if (abb == "AR")
        return 4;
    if (abb == "CA")
        return 5;
    if (abb == "CO")
        return 6;
    if (abb == "CT")
        return 7;
    if (abb == "DE")
        return 8;
    if (abb == "FL")
        return 9;
    if (abb == "GA")
        return 10;
    if (abb == "HI")
        return 11;
    if (abb == "ID")
        return 12;
    if (abb == "IL")
        return 13;
    if (abb == "IN")
        return 14;
    if (abb == "IA")
        return 15;
    if (abb == "KS")
        return 16;
    if (abb == "KY")
        return 17;
    if (abb == "LA")
        return 18;
    if (abb == "ME")
        return 19;
    if (abb == "MD")
        return 20;
    if (abb == "MA")
        return 21;
    if (abb == "MI")
        return 22;
    if (abb == "MN")
        return 23;
    if (abb == "MS")
        return 24;
    if (abb == "MO")
        return 25;
    if (abb == "MT")
        return 26;
    if (abb == "NE")
        return 27;
    if (abb == "NV")
        return 28;
    if (abb == "NH")
        return 29;
    if (abb == "NJ")
        return 30;
    if (abb == "NM")
        return 31;
    if (abb == "NY")
        return 32;
    if (abb == "NC")
        return 33;
    if (abb == "ND")
        return 34;
    if (abb == "OH")
        return 35;
    if (abb == "OK")
        return 36;
    if (abb == "OR")
        return 37;
    if (abb == "PA")
        return 38;
    if (abb == "RI")
        return 39;
    if (abb == "SC")
        return 40;
    if (abb == "SD")
        return 41;
    if (abb == "TN")
        return 42;
    if (abb == "TX")
        return 43;
    if (abb == "UT")
        return 44;
    if (abb == "VT")
        return 45;
    if (abb == "VA")
        return 46;
    if (abb == "WA")
        return 47;
    if (abb == "WV")
        return 48;
    if (abb == "WI")
        return 49;
    if (abb == "WY")
        return 50;
    if (abb == "DC")
        return 51;
}

function LoadZip(zip, cityID, stateID) {
    var address = LoadAddress(zip);

    if (address != null) {
        var elem1 = document.getElementById(cityID);
        var l1 = elem1.selectedIndex;
        elem1.options.length = address.city.length;
        for (i = 0; i < address.city.length; i++) {
            elem1.options[i] = new Option(address.city[i].replace('\n', '').replace('\r', ''));
            elem1.options[i].setAttribute('value', address.city[i].replace('\n', '').replace('\r', ''));
            elem1.options.selectedIndex = 0;
        }

        if (stateID != null) {
            var stateValue = GetStateID(address.state);

            var stateList = document.getElementById(stateID);
            setDropDownList(stateList, stateValue);
        }
    }
}
function setDropDownList(elementRef, valueToSetTo)
{
    for (var i=0; i<elementRef.options.length; i++)
    {
        if ( elementRef.options[i].value == valueToSetTo )
        {
            elementRef.options[i].selected = true;
        }
    }
}

function CheckZip() {
    var elem = document.getElementById('Zip');
    if (elem.value.length == 5) {
        console.log("CheckZip");
        LoadZip(elem.value, 'City', 'State');
        var city = document.getElementById('City');
        var st = document.getElementById('State');
        var haveCity = false;
        var haveSt = false;
        if (city.options.length > 0 && city.options[city.selectedIndex].value.trim() !== '') {
            city.style.backgroundColor = valColor;
            haveCity = true;
        }
        if (st.options.length > 0 && st.options[st.selectedIndex].value.trim() !== '') {
            st.style.backgroundColor = valColor;
            haveSt = true;
        }
        if (haveSt && haveCity) {
            elem.style.backgroundColor = valColor;
        }
    }
}