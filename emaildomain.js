function AppendDDLToEM(str, obj, fld, ed) {
	var ed = ed;
	var val = obj.options[obj.selectedIndex].value;
	var arrEM = [document.getElementById(fld)];
	if (arrEM.length === 1) {
		var ems = arrEM[0].value.split(',');
		var em = ems.length > 0 ? ems[ems.length - 1] : "";
		var li = em.lastIndexOf(str) == -1 ? em.length : em.lastIndexOf(str);
		li = (li < em.lastIndexOf('@') || em.lastIndexOf('@') == -1) && em.substring(em.length - 1) != str ? em.length : li;
		em = em.substring(0, li) + val;
		var emails = "";
		for (var i = 0; i < ems.length - 1; i++) {
			if (i > 0)
				emails += ",";

			emails += ems[i];
		}
		if (emails.length > 0)
			em = emails + "," + em;
		arrEM[0].value = em;

		//****************************************
		//some tablets needs this to work properly
		arrEM[0].blur();
		//****************************************
	}
	rebuildDDLEMDomain(ed);
}

function rebuildDDLEMDomain(ed) {
	var ed = ed;

	var arr = new Array("@", "", "@gmail.com", "@gmail.com", "@yahoo.com", "@yahoo.com", "@hotmail.com", "@hotmail.com", "@aol.com", "@aol.com", "@msn.com", "@msn.com");
	$('#' + ed).empty();

	for (var i = 0; i < arr.length; i = i + 2) {
		addOption2(arr[i], arr[i + 1], i == 0, ed);
	}
	document.getElementById(ed).options[0].disabled = true;
}
function addOption2(textString, valueSetting, currentValue, ed) {
	$('#' + ed).append($('<option>', {
		value: valueSetting,
		text: textString,
		selected: currentValue
	}));
}