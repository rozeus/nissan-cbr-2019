var flag = false;

var show = { 
    forward: {
        p: {opacity: [ 1, 0 ],/* translateX: [ 0, "100%" ],*/ translateZ: 0},
        o: {duration: 250, display:'block'}
    },
    back: {
        p: {opacity: [ 1, 0 ],/* translateX: [ 0, "-100%" ],*/ translateZ: 0},
        o: {duration: 250, display:'block'}
    }
};

var hide = { 
    forward: {
        p: {opacity: [ 0, 1 ],/* translateX: [ "-100%", 0 ],*/ translateZ: 0},
        o: {duration: 250, display:'none'}
    },
    back: {
        p: {opacity: [ 0, 1 ],/* translateX: [ "100%", 0 ],*/ translateZ: 0},
        o: {duration: 250, display:'none'}
    }
};

function showHideView(objHide, objShow, direction) {
window.scrollTo(0,0);
    if (!flag) {

        flag = true;

        if (direction === 'forward') {
            objHide.velocity(hide.forward.p, hide.forward.o);
            objShow.velocity(show.forward.p, show.forward.o);
        }

        if (direction === 'back') {
            objHide.velocity(hide.back.p, hide.back.o);
            objShow.velocity(show.back.p, show.back.o); 
        }

        var flagTimer = setTimeout(function () {
            clearTimeout(flagTimer);
            flag = false;
        }, 600)

    }

};